﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_13
{
    class Matrix
    {
        private int m;
        private int n;
        double[,] matrix = new double[1,1];
        public Matrix(int _m, int _n)
        {
            m = _m;
            n = _n;
            matrix = new double[m,n];
        }

        public Matrix(double[,]matr)
        {
            m = matr.GetLength(0);
            n = matr.GetLength(1);
            matrix = matr;
        }
        public Matrix(Matrix a)
        {
            m = a.m;
            n = a.n;
            matrix = a.matrix;
        }
        public int M
        {
            get{ return m; }
            //set{ m = value;}
        }
        public int N
        {
            get { return m; }
            //set { m = value; }
        }
        public double[,] MATR
        {
            get { return matrix; }
        }
        public void GetRandomMatrix()
        {
            Random rnd = new Random();
            for(int i=0;i< m;i++)
            {
                for(int o=0;o< n;o++)
                {
                    matrix[i,o] = rnd.Next(0, 101);
                }
            }   
        }
        public Matrix MUL(Matrix B)
        {
            Matrix A = new Matrix(1, 1);
            int _m = B.M;
            int _n = B.N;
            if (_n != m)
            {
                Console.WriteLine("Кількість стовпчиків матриці А не співпадає з кількістю рядків матриці В");
            }
            else
            {
                double[,] C = new double[m,_n];
                for (int i = 0; i < m; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        for (int k = 0; k < _n; k++)
                        {
                            C[i,j] += matrix[i,k] * B.matrix[k,j];
                        }
                    }
                }
                A.GetMatrix(C);
            }
            return A;
        }
        public void Write()
        {
            for (int _m = 0; _m < m; _m++)
            {
                for (int _n = 0; _n < m; _n++)
                {
                    Console.Write(matrix[_m, _n]+" ");
                }
                Console.WriteLine();
            }
        }
        
        public void GetMatrix(double[,] _matr)
        {
            matrix = _matr;
            m = matrix.GetLength(0);
            n = matrix.GetLength(1);
        }

        public Matrix Transponovana()
        {
            Matrix tmp = new Matrix(1,1);
            double[,] mas = new double[n,m];
            try
            {
                for (int _n = 0; _n < n; _n++)
                {
                    for (int _m = 0; _m < m; _m++)
                    {
                        mas[_n, _m] = matrix[_m, _n];
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("error");
            }
            tmp.GetMatrix(mas);
            return tmp;
        }


        public double Viznachnik()
        {
            if (this.m != 1 && this.n != 1)
            {
                return DetRec(matrix);
            }
            else
            {
                return matrix[0, 0];
            }
        }

        public double[,] GF()
        {
            double[] Y = new double[4];
            double[] P = new double[4];
            double[,] V = matrix;
            double[,] C = matrix;

            for (int k = 0; k < n; k++)
            {
                Y[k] = P[k] / V[k,k];
                for (int i = k + 1; i < n; i++)
                {
                    P[i] += -V[i,k] * Y[k];
                    for (int j = k + 1; j < n; j++)
                    {
                        C[k,j] = V[k,j] / V[k,k];
                        V[i,j] += -V[i,k] * C[k,j];
                    }
                }
            }

            double[] X = new double[4];
            for(int i = 0; i < n; i++)
            X[n - 1] = Y[n - 1];
            for (int i = n - 2; i >= 0; i--)
            {
                for (int j = i + 1; j < n; j++)
                    X[i] += C[i,j] * X[j];
                X[i] = Y[i] - X[i];
            }
            return V;
        }

        private double DetRec(double[,] matrix)
        {
            if (matrix.Length == 4)
            {
                return matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
            }
            double sign = 1, result = 0;
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                double[,] minor = GetMinor(matrix, i);
                result += sign * matrix[0, i] * DetRec(minor);
                sign = -sign;
            }
            return result;
        }

        private double[,] GetMinor(double[,] matrix, double n)
        {
            double[,] result = new double[matrix.GetLength(0) - 1, matrix.GetLength(0) - 1];
            for (int i = 1; i < matrix.GetLength(0); i++)
            {
                for (int j = 0, col = 0; j < matrix.GetLength(1); j++)
                {
                    if (j == n)
                        continue;
                    result[i - 1, col] = matrix[i, j];
                    col++;
                }
            }
            return result;
        }

        private double[,] Minor(double[,] matrix, int m, int n)
        {
            int lic1 = 0, lic2=0;
            double[,] result = new double[matrix.GetLength(0) - 1, matrix.GetLength(0) - 1];
            for(int _m=0;_m< matrix.GetLength(0);_m++)
            {
                lic2 = 0;
                for (int _n=0;_n< matrix.GetLength(0);_n++)
                {
                    if (m == _m || n == _n) { continue; }
                    result[lic1,lic2] = matrix[_m, _n];
                    lic2++;
                }
                if (m != _m) lic1++;
            }
            return result;
        }


        public Matrix OBERNENA()
        {
            double[,] mattr = this.matrix;
            double[,] result = this.matrix;
            double det = this.Viznachnik();
            Matrix tmp_matr = this.Transponovana();
            for(int i=0;i< m;i++)
            {
                for(int o=0;o< n;o++)
                {
                    mattr = (Minor(tmp_matr.MATR, i,o));
                    Matrix az = new Matrix(mattr);
                    result[i, o] = ((Math.Pow(-1,i+1+o+1))*az.Viznachnik()*(1/det));
                }
            }
            tmp_matr.GetMatrix(result);
            return tmp_matr;
        }

    }
}
