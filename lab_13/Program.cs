﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_13
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix mr = new Matrix(2, 2);
            mr.GetRandomMatrix();
            Console.WriteLine("Matrix a");
            mr.Write();
            Console.WriteLine("Transponovana");
            Matrix mr_tr = mr.Transponovana();
            mr_tr.Write();

            Matrix mr2 = new Matrix(2, 2);
            mr2.GetRandomMatrix();
            Console.WriteLine("Matrix b");
            mr2.Write();
            Console.WriteLine("Matrix a * Matrix b");
            Matrix mr3 = mr.MUL(mr2);
            mr3.Write();
            Console.WriteLine("Viznachnik a = "+ mr.Viznachnik());
            Console.WriteLine("Zvorotna a");
            Matrix mr_zv = mr.OBERNENA();
            mr_zv.Write();
        }
    }
}
